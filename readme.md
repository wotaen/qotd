Wagtail sample project with Gitlab's CI and EBS
===============================================

Demo: http://qotd-env.eu-west-1.elasticbeanstalk.com
`admin / aaaad34sfs23DSfaa`  

This is a very basic setup of Wagtail's CMS on EBS deployed
via Gitlab's CI.

Local dev
---------
1) Clone this repo
2) Update `qotd/settings/dev.py`
3) Run it either via `runserver` or via Docker `docker build -t qotd . && docker run -p 80:80 qotd`

Production
----------
1) Make your changes, and push to repo...a few minutes later, the application will be updated

Things to improve
=================

I won't discuss/comment on Django part of things. 
I built a simple app with Wagtail CMS to show Quote Of The Day (it can be managed from admin). After that, the app just became a black box :)

This is a proof-of-concept type of work and I acknowledge the following things that could be improved:

* DB password (or RDS) should be passed via env, it's exposed in the repository. Also the entire
RDS is publicly available. In reality, only internal access from a specific IP should be allowed
* I'm undetermined whether `./manage.py migrate` should be inside `Dockerfile`.
* Much improved logging. EBS collects logs from containers and exposes them on the host. I had an issue with clashing paths
for the internal image's `nginx` and EBS. I solved by changing the log path in `Dockerrun.aws.json` to `"Logging": "/var/log-ebs"`
* Figure out how to use latest version of `awsebcli` instead of the older one
* Use Amazon's own docker registry. I originally intended to do that, but ran into issues and decided that this proof-of-concept
does not depend on repo location as long as that repo is private. Not an issue, but I think the EBS deploy could go a little bit faster.
* `nginx.conf` is bound to `qotd-env.eu-west-1.elasticbeanstalk.com`, so if the DNS changes site will not load.
* Work on `.gitlab-ci.yml` a bit so that not every commit, updating this readme in particular, don't end up with launching new version :)


Concepts etc...
===============

A brief description of the project setup + a few gotchas that took some time to figure out

Docker image
------------
* Runs migrations, collects static and manages `nginx` and `gunicorn` instances
with `supervisor`

* `ENV DJANGO_SETTINGS_MODULE qotd.settings.production` is to be used instead of `ENV DJANGO_ENV production` provided
in the repository clone (outdated version I guess?)

.gitlab-ci.yml
--------------
* AWS credentials are provided via Gitlab's enviroment variables section
* This bit `pip install 'awsebcli==3.7.4' --force-reinstall` took some time figuring out. It turns out that the current version (3.12) didn't work for some reason. According to some forum posts there were other people having the same issue and version 3.7 worked well.
* The section around creating and filling `~/.aws/config` with credentials is necessary so that `awsebcli` can deploy to EBS. `awscli` can use env variables for authentication, but I don't think that `awsebcli` can do the same, hence this file

nginx
-----
* Serves static content generated with `staticfiles` and forwards everything else to `gunicorn`
 
gunicorn
--------
* Just copied the setup from original repository source, so nothing fancy here

Prep work on local machine
--------------------------------

**EBS**

- install `awsebcli`

- `eb init`, choose an existing EBS application or create a new one. This will create `.elasticbeanstalk/config.yml` file

- notice this line in the `config.yml file`:
```
deploy:
  artifact: Dockerrun.aws.json
```
We're telling EBS to deploy using this file only, not via zipping the entire content's of the root dir

- `./Dockerrun.aws.json` file - This file tells EBS where to look for the docker image and where to get credentials for it. I'm using gitlab's repository storage. See here how to obtain storage credentials:
https://docs.gitlab.com/ce/user/project/container_registry.html. The format then needs to be changed a bit (because AWS uses an older format) so that it looks like this:
```
{
	"registry.gitlab.com": {
		"auth": "<auth>",
		"email": "holub.michal@gmail.com"
		}
	}
}
```

* Make sure to upload credentials file to S3 container AND allow GetObject permission for EBS's application role