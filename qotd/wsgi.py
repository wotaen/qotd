"""
WSGI config for qotd project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.0/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application

settings = os.environ.get('DJANGO_ENV','dev')

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "qotd.settings." + settings)

application = get_wsgi_application()
