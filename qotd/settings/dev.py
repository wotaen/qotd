from .base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'j!(0p940v2nsv)x$kpjef%++!wdbaj5h0mh_$*lxlz=1tkkbza'


EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'qotd',
        'USER': 'chytra_skola_admin',
        'PASSWORD': 'aaaaaa',
        'HOST': 'localhost',
        'PORT': '5432',
    },
}

AUTH_PASSWORD_VALIDATORS = [

]

try:
    from .local import *
except ImportError:
    pass
