from .base import *

DEBUG = True

SECRET_KEY = 'j!(0p940v2nsv)x$kpjef%++!wdbaj5h0mh_$*lxlz=1tkkbza'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'qotd',
        'USER': 'qotd_admin',
        'PASSWORD': 'aaaad34sfs23DSfaa',
        'HOST': 'qotd.cqbb62nqak0i.eu-west-1.rds.amazonaws.com',
        'PORT': '5432',
    },
}

BASE_URL = 'http://qotd-env.eu-west-1.elasticbeanstalk.com'
ALLOWED_HOSTS = ['*']

try:
    from .local import *
except ImportError:
    pass
