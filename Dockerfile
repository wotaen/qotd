FROM python:3.6
LABEL maintainer="holub.michal@gmail.com"

ENV PYTHONUNBUFFERED 1
# ENV DJANGO_ENV production
ENV DJANGO_SETTINGS_MODULE qotd.settings.production

RUN apt-get update && apt-get install -y --no-install-recommends \
        nginx \
        supervisor \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    && apt-get autoremove -y --purge


COPY ./requirements.txt /code/requirements.txt
RUN pip install -r /code/requirements.txt
RUN pip install gunicorn
RUN echo "daemon off;" >> /etc/nginx/nginx.conf
RUN chmod 777 /var/log/supervisor/ && touch /var/log/supervisor/supervisor.log
COPY supervisor.conf /etc/supervisor/conf.d/supervisor.conf
COPY nginx.conf /etc/nginx/conf.d/qotd.conf
COPY . /code/
WORKDIR /code/
COPY entrypoint.sh ./

RUN python manage.py migrate
RUN python manage.py collectstatic --noinput
RUN rm /etc/nginx/sites-enabled/default

ENTRYPOINT ["./entrypoint.sh"]