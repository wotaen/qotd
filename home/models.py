from django.db import models
from wagtail.admin.edit_handlers import FieldPanel
from wagtail.core.fields import RichTextField

from wagtail.core.models import Page
from wagtail.snippets.models import register_snippet


class HomePage(Page):
    body = RichTextField(blank=True)

    content_panels = Page.content_panels + [
        FieldPanel('body', classname="full"),
    ]

@register_snippet
class Quote(models.Model):
    quote = models.TextField()
    author = models.CharField(max_length=256)
    tech = models.CharField(max_length=256)

    panels = [
        FieldPanel('quote'),
        FieldPanel('author'),
        FieldPanel('tech'),
    ]

    def __str__(self):
        return self.quote