from django import template

from home.models import Quote

register = template.Library()

# Advert snippets
@register.inclusion_tag('home/quotes.html', takes_context=True)
def quotes(context):
    quote = Quote.objects.order_by('?').first()
    return {
        'quote': quote,
        'request': context['request'],
    }